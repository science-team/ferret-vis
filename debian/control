Source: ferret-vis
Section: utils
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Alastair McKinstry <mckinstry@debian.org>
Build-Depends: debhelper-compat (= 13),
 dh-elpa (>=2.1.5),
 libeditreadline-dev, 
 libcurl4-openssl-dev | libcurl-dev, 
 libxgks-dev (>= 2.6.1+dfsg.2-14.1),
 libncurses-dev, 
 gfortran | fortran-compiler, 
 libnetcdf-dev, libnetcdff-dev,
 libxmu-headers, 
 csh, 
 libxt-dev 
Standards-Version: 4.7.0
Homepage: https://ferret.pmel.noaa.gov/Ferret/home
Vcs-Browser: https://salsa.debian.org/science-team/ferret-vis.git
Vcs-Git: https://salsa.debian.org/science-team/ferret-vis.git -b debian/latest

Package: ferret-vis
Architecture: any
Depends: ${misc:Depends}, 
 ${shlibs:Depends},
 dh-elpa,
 ${elpa:Depends}, 
 tcsh | csh | c-shell
Recommends: ferret-datasets, 
 python3-ferret
Description: Interactive data visualization and analysis environment
 Ferret is an interactive computer visualization and analysis environment
 designed to meet the needs of oceanographers and meteorologists analyzing 
 large and complex gridded data sets. It can transparently access extensive
 remote Internet data bases using OPeNDAP (formerly known as DODS)
 .
 Ferret has a Mathematica-like flexibility, geophysical formatting, 
 "intelligent" connection to its data base, memory management for very large 
 calculations, and symmetrical processing in 4 dimensions. It can work on both
 gridded and non-gridded datasets.

 
Package: ferret-datasets
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: ferret-vis
Description: Datasets for use with Ferret Visualisation and analysis suite
 These datasets contain reference climatologies and grids for use with ferret.
 They include:
  * etopo120            relief of the earth's surface at 120-minute resolution
  * etopo60             relief of the earth's surface at 60-minute resolution
  * levitus_climatology subset of the Climatological Atlas of the World Oceans 
                        by Sydney Levitus (Note: the updated World Ocean Atlas,
                        1994, is also available with Ferret)
  * coads_climatology   12-month climatology derived from 1946–1989 of the 
                        Comprehensive Ocean/Atmosphere Data Set
  * monthly_navy_winds  monthly-averaged Naval Fleet Numerical Oceanography 
                        Center global marine winds (1982–1990)
  * esku_heat_budget    Esbensen-Kushnir 4×5 degree monthly climatology of the
                        global ocean heat budget (25 variables)
