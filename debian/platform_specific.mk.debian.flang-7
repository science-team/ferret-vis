#
# platform_specific.mk.debian
#
# This file is included in other Makefiles and defines
# platform specific macros

PLATFORM = $(shell uname -s -r)

GUI_INCLUDES    =
GKS_INCLUDES    =-I/usr/include/xgks 
INCLUDES        = \
       -I../../fmt/cmn \
       -I../../fer/common \
       -I/$(NETCDF4_DIR)/include \
       -I/usr/include \
       -I/usr/include/X11 \
       -I../../ppl/include\
       -I../../ppl/tmap_inc \
		-I/usr/include/xgks
FINCLUDES       =  -I../../ppl/tmap_inc \
       -I../../fmt/cmn \
       -I../../ppl/include \
       -I../../fer/common \
       -I../../external_functions/ef_utility

CC      = gcc
CXX		= c++
FC		= flang
F77		= flang
RANLIB  = /usr/bin/ranlib
CPP		= /lib/cpp
LD		= gcc
LDFLAGS		= -v --verbose -fpic -export-dynamic
LD_DYN_FLAGS = -v -fpic -shared

CPP_FLAGS       = $(INCLUDES) \
			  -Dunix -Dgfortran   \
			  -DNO_OPEN_SHARED \
			  -DNO_OPEN_RECORDTYPE \
			  -DX_REFRESH -Dreclen_in_bytes  \
			  -DNO_OPEN_READONLY -DMANDATORY_FORMAT_WIDTHS\
			  -DNO_OPEN_CARRIAGECONTROL -Dxgks -DSTAR_1_SUPPORTED \
			  -DFULL_GUI_VERSION -DX_REFRESH \
			  -DXT_CODE  -DLINUX -DNO_PASSED_CONCAT \
			  -Dcrptd_cat_argument \
			  -DG77_SIGNAL -DG77 \
			  -DNEED_IAND -DINTERNAL_READ_FORMAT_BUG \
			  -DNO_PREPEND_STRING -DNO_DOUBLE_ESCAPE_SLASH \
			  -Ddouble_p 

# Flags for compiling all C code
# -DusingDODSf2cUnderscore needed if using netcdf library...
# also consider -ffloat-store.
CFLAGS		= $(CPP_FLAGS) -fPIC  \
		  -Dlint -D_SSIZE_T -DVOID_SIGHANDLER -D_POSIX_VERSION -DLINUX \
		  -DFULL_GUI_VERSION -DX_REFRESH -DXT_CODE -Dsun4

# Flags for compiling the PlotPlus FORTRAN code (ppl subdirectory)
PPLUS_FFLAGS	= $(CPP_FLAGS) \
		  -fdollars-in-identifiers -ffixed-line-length-132 -fno-backslash $(FINCLUDES)
# Flags for compiling non-PlotPlus FORTRAN code
FFLAGS		= $(CPP_FLAGS) -fPIC  \
		  -fdollars-in-identifiers -ffixed-line-length-132 -fno-backslash \
		  -fdefault-real-8  $(FINCLUDES)

# Add hardening
CFLAGS += `dpkg-buildflags --get CFLAGS`
CPPFLAGS += `dpkg-buildflags --get CPPFLAGS`
# Hardening, but remove -g due to known bug in flang
FFLAGS += $(shell dpkg-buildflags --get FFLAGS |  sed -e 's/-g //')

# PROF_FLAGS	= -pg

GKSLIB		= -lxgks-flang
SYSLIB		= -lX11 -lm -ldl
CDFLIB 		= -lnetcdff -lnetcdf 
TERMCAPLIB	=
READLINELIB	= -lreadline

LINUX_OBJS	= special/linux_routines.o \
			  dat/*.o \
			  ../fmt/src/x*.o \
			  ../ppl/plot/ppldata.o

## cancel the default rule for .f -> .o to prevent objects from being built
## from .f files that are out-of-date with respect to their corresponding .F file
#%.o : %.f
#
## use cpp to preprocess the .F files to .f files and then compile the .f files
#%.o : %.F
#	rm -f $*.f
#	$(CPP) -P -traditional $(CPP_FLAGS) $(<F) | sed -e 's/de    /de /g' | sed -e 's/de         /de /g' > $*.f
#	$(F77) $(FFLAGS) -c $*.f	 

# Directly compile the .F source files to the .o object files
# since gfortran can handle the C compiler directives in Fortran code
%.o : %.F
	$(FC) $(FFLAGS) -c $*.F -o $*.o

#
# End of platform_specific_includes.mk.debian
#
