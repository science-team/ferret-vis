#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
export DH_VERBOSE=1

# Set for reproducibility
LC_ALL=C
export LC_ALL

# magic debhelper rule
%:
	dh $@ --with elpa

FER_EXT:=$(CURDIR)/debian/ferret-vis/usr/lib/ferret-vis/external_functions
ARCH:=$(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
LIBDIR:=/usr/lib/${ARCH}
INSTALLDIR:=$(CURDIR)/debian/tmp
# FC set to f77 by default in make
# Read default compiler name unless FC is actually set
ifeq ($(FC),f77)
  FC:=$(shell basename $(shell readlink /etc/alternatives/f95))
endif

# Version of Fortran compiler
FORT_VER:=$(shell basename $(shell readlink /usr/bin/$(FC)))

site_specific.mk:
	sed -e 's%@ARCH@%${ARCH}%' \
		-e 's%@LIBDIR@%${LIBDIR}%' \
		-e 's%@INSTALLDIR@%${INSTALLDIR}%' \
		< debian/site_specific.mk.in > site_specific.mk
	cp debian/platform_specific.mk.debian.$(FORT_VER) platform_specific.mk.debian
	cp site_specific.mk platform_specific.mk.debian  external_functions/ef_utility

override_dh_auto_build: site_specific.mk
	$(MAKE) -C fer HOSTTYPE=debian update all
	-$(MAKE) -C external_functions HOSTTYPE=debian
	$(MAKE) -C gksm2ps debian

override_dh_install:
	dh_installdirs
	mkdir -p  $(FER_EXT)
	-$(MAKE) -C external_functions install HOSTTYPE=debian \
		FER_LOCAL_EXTFCNS=$(FER_EXT) FER_DIR=$(CURDIR)/debian/tmp
	dh_install
	# Drop extension (allow to fail on binary-only build)
	-mv debian/ferret-vis/usr/bin/Fpatch.csh debian/ferret-vis/usr/bin/Fpatch

override_dh_elpa_test:
	@echo pass tests

override_dh_dwz:
	@echo "DWZ bypassed in this build: issues with dwz on mips"

override_auto_dh_test:
	(cd bench && ./run_all )

override_dh_auto_clean: 
	# use system xpm.h, not supplied ones
	rm -f fer/gui/xpm*.h
	find . -type l  -delete
	find . -name '*.o' -delete
	find . -name '*.so' -delete
	find external_functions -type l -exec rm {} \;
	[ ! -f site_specific.mk ] || $(MAKE) -C gksm2ps clean
	[ ! -f fer/platform_specific_flags.mk.debian ]  || $(MAKE) -C fer clean HOSTTYPE=debian 
	rm -rf lib gksm2ps/gksm2ps fer/ferret_c*

